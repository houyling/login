import 'package:flutter/material.dart';
import 'package:login_project/components/text_field_container.dart';
import 'package:login_project/constrant.dart';

class RoundedPasswordField extends StatelessWidget {
  final ValueChanged<String> onChanged;

  // void showPassword() {}
  const RoundedPasswordField({
    Key? key,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        // onTap: () => print('TextField onTap'),
        obscureText: true, // hide the text
        onChanged: onChanged,
        decoration: InputDecoration(
          hintText: "Password",
          icon: Icon(
            Icons.lock_outline_rounded,
            color: kPrimaryColor,
          ),
          suffixIcon: Icon(
            Icons.visibility,
            color: kPrimaryColor,
          ),
          // suffixIcon: IconButton(
          //   icon: Icon(Icons.visibility),
          //   color: kPrimaryColor,
          //   onPressed: () => print('suffixIcon pressed'),
          //   // onPressed: showPassword,
          // ),

          border: InputBorder.none,
        ),
      ),
    );
  }
}
