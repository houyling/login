import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_project/constrant.dart';

class CardBox extends StatelessWidget {
  final String titleText;
  final String subtitleText;
  final IconData leadingIcon;
  final String leadingImage;
  final String trailingText; // status text
  final Color trailingColor; // status color
  final bool is_icon; // status color

  //  = 'assets/icons/delivery_done.svg';

  const CardBox({
    Key? key,
    required this.titleText,
    this.subtitleText = '',
    this.leadingIcon = Icons.train,
    this.trailingText = 'Trainsmit',
    this.trailingColor = Colors.orange,
    this.leadingImage = "assets/icons/delivery_done.svg",
    this.is_icon = true,
  }) : super(key: key);

// leadingIcon = Icons.train_outlined
  // Colors.orange
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    Widget svg = SvgPicture.asset(
      leadingImage,
      height: size.height * 0.05,
    );

    return Card(
      // margin: EdgeInsets.all(8),
      child: ListTile(
        horizontalTitleGap: 10,
        leading: is_icon
            ? Icon(
                leadingIcon,
                size: 35,
                color: kPrimaryColor,
              )
            : svg,
        title: Text(
          titleText,
          style: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle: Text(
          subtitleText,
          style: TextStyle(
            fontSize: 13,
          ),
        ),
        // trailing: Icon(Icons.more_vert),
        trailing: Text(
          trailingText,
          style: TextStyle(fontSize: 13, color: trailingColor),
        ),
      ),
    );
  }
}
