import 'package:flutter/material.dart';
import 'package:login_project/constrant.dart';

class RoundedButton extends StatelessWidget {
  const RoundedButton({
    Key? key,
    required this.text,
    required this.onPress,
    this.color = kPrimaryColor,
    this.textColor = Colors.white,
  }) : super(key: key);

  final String text;
  // final Function onPress;
  final VoidCallback
      onPress; // use this to define the void function for the button
  final Color color, textColor;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12),
      width: size.width * 0.79,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(29),
        child: newElveateButton(),
      ),
    );
  }

  //Used:ElevatedButton as FlatButton is deprecated.
  //Here we have to apply customizations to Button by inheriting the styleFrom

  Widget newElveateButton() {
    return ElevatedButton(
      child: Text(
        text,
        style: TextStyle(color: textColor),
      ),
      // onPressed: () => onPress, // cannot use like this, it's not work
      onPressed: onPress,
      style: ElevatedButton.styleFrom(
        primary: color,
        padding: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
        textStyle: TextStyle(
            color: textColor, fontSize: 14, fontWeight: FontWeight.w500),
      ),
    );
  }
}
