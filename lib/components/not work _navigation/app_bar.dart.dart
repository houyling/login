import 'package:flutter/material.dart';
import 'package:login_project/constrant.dart';

// class AppNavigationBar extends StatelessWidget {
//   final Widget child;
//   const AppNavigationBar({
//     Key? key,
//     required this.child,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;
//     return Container(
//       margin: EdgeInsets.symmetric(vertical: 10),
//       padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
//       width: size.width * 0.8,
//       decoration: BoxDecoration(
//         color: kPrimaryLightColor,
//         borderRadius: BorderRadius.circular(29),
//       ),
//       child: child,
//     );
//   }
// }

class TopNavigationBar extends StatelessWidget with PreferredSizeWidget {
  @override
  final String title;
  final Icon icon;

  // final VoidCallback
  // onPress; // use this to define the void function for the button
  final Color color, textColor;

  TopNavigationBar({
    Key? key,
    required this.title,
    this.icon = const Icon(Icons.home, size: 30),
    // required this.onPress,
    this.color = kPrimaryColor,
    this.textColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: kPrimaryColor,
      automaticallyImplyLeading: false, // remove the leading icon
      title: Row(
        children: [
          // IconButton(
          //   icon: icon,
          //   color: color,
          //   // onPressed: onPress,
          // ),
          icon,
          Text(
            title,
            style: TextStyle(
              fontSize: 22,
            ),
          ),
        ],
      ),
      // actions: <Widget>[
      //   IconButton(
      //     icon: const Icon(Icons.notifications, size: 30),
      //     tooltip: 'Show Snackbar',
      //     onPressed: () {
      //       ScaffoldMessenger.of(context).showSnackBar(
      //           const SnackBar(content: Text('This is a snackbar')));
      //     },
      //   ),
      // ],
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => throw UnimplementedError();
}
















// AppBar(
//       backgroundColor: kPrimaryColor,
//       automaticallyImplyLeading: false, // remove the leading icon
//       title: Row(
//         children: [
//           IconButton(
//             icon: const Icon(Icons.home, size: 30),
//             tooltip: 'Show Snackbar',
//             onPressed: () {
//               // notification flash
//               // ScaffoldMessenger.of(context).showSnackBar(
//               //     const SnackBar(content: Text('This is a snackbar')));
//             },
//           ),
//           Text(
//             'Home',
//             style: TextStyle(
//               fontSize: 22,
//             ),
//           ),
//         ],
//       ),
//       actions: <Widget>[
//         // SizedBox(
//         //     height: size.height * 0.02), // add margin after the text above
//         IconButton(
//           icon: const Icon(Icons.notifications, size: 30),
//           tooltip: 'Show Snackbar',
//           onPressed: () {
//             ScaffoldMessenger.of(context).showSnackBar(
//                 const SnackBar(content: Text('This is a snackbar')));
//           },
//         ),
//       ],
//     );