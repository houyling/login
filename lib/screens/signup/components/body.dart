import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_project/components/already_have_an_account.dart';
import 'package:login_project/components/rounded_button.dart';
import 'package:login_project/components/rounded_input_field.dart';
import 'package:login_project/components/rounded_password_field.dart';
import 'package:login_project/screens/login/login_screen.dart';
import 'package:login_project/screens/signup/components/background.dart';
import 'package:login_project/screens/signup/components/or_divider.dart';
import 'package:login_project/screens/signup/components/social_icon.dart';

class Body extends StatelessWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // TextEditingController emailController = new TextEditingController();

    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "SIGNUP",
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
                height: size.height * 0.02), // add margin after the text above
            SvgPicture.asset(
              "assets/icons/signup.svg",
              height: size.height * 0.35,
            ),
            SizedBox(
                height: size.height * 0.02), // add margin after the text above
            RoundedInputField(
              hintText: "Email",
              icon: Icons.person_outline_rounded,
              // textInput:
              //     emailController, // TODO: need to get the text from input
              onChanged: (value) => {},
            ),
            RoundedPasswordField(
              onChanged: (value) => {},
            ),
            RoundedButton(
              text: "Signup",
              onPress: () => {},
            ),
            SizedBox(
                height: size.height * 0.01), // add margin after the text above
            AlreadyHaveAnAccountCheck(
              login: false,
              press: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => LoginScreen(),
                  ),
                ),
              },
            ),
            OrDivider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SocialIcon(
                  iconSrc: "assets/icons/facebook.svg",
                  press: () => {},
                ),
                SocialIcon(
                  iconSrc: "assets/icons/twitter.svg",
                  press: () => {},
                ),
                SocialIcon(
                  iconSrc: "assets/icons/google-plus.svg",
                  press: () => {},
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
