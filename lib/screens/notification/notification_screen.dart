import 'package:flutter/material.dart';
import 'package:login_project/constrant.dart';
import 'package:login_project/screens/home/home_screen.dart';
// import 'package:curved_navigation_bar/curved_navigation_bar.dart';
// import 'package:login_project/screens/home/home_screen.dart';
import 'package:login_project/screens/notification/components/body.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);
  @override
  _NotificationScreen createState() => _NotificationScreen();
}

class _NotificationScreen extends State<NotificationScreen> {
  //State class
  // int _page = 0;
  // GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();

// This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    // Body();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        // automaticallyImplyLeading: false, // remove the leading icon
        centerTitle: true, // set the title to center
        title: const Text('Notification'),
        // title: Center(
        //   child: new Text('User Profile', textAlign: TextAlign.center), // this is not center,
        // ),
        leading: IconButton(
          icon: Icon(Icons.keyboard_arrow_left_rounded, size: 30),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => HomeScreen(),
              ), // navigate to the login screen
            );
          },
        ),
      ),
      body: Body(),
    );
  }
}
