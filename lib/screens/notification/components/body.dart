// import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_project/constrant.dart';
// import 'package:login_project/components/rounded_button.dart';
import 'package:login_project/screens/user_profile/components/background.dart';

class Body extends StatelessWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      // child: SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Stack(
            children: [
              Positioned(
                // left: 0,
                // bottom: 5,
                child: Container(
                  // padding: EdgeInsets.symmetric(vertical: 80, horizontal: 10),
                  padding: EdgeInsets.only(top: 80.0),
                  alignment: Alignment.center,
                  height: size.height * 0.65,
                  width: size.width * 0.9,
                  child: ListView(
                    // scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    children: const <Widget>[
                      // Card(child: ListTile(title: Text('One-line ListTile'))),
                      Text(
                        "User Information",
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Card(
                        child: ListTile(
                          leading: Icon(
                            Icons.person,
                            size: 30,
                            color: kPrimaryColor,
                          ),
                          title: Text(
                            'Name',
                            style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          subtitle: Text(
                            'Selena Gomez',
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          leading: Icon(
                            Icons.phone,
                            size: 30,
                            color: kPrimaryColor,
                          ),
                          title: Text(
                            'Phone',
                            style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          subtitle: Text(
                            '012 123 456 / 011 123 456',
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          leading: Icon(
                            Icons.email,
                            size: 30,
                            color: kPrimaryColor,
                          ),
                          title: Text(
                            'Email',
                            style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          subtitle: Text(
                            'selena.gomez@gmail.com',
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          leading: Icon(
                            Icons.location_on_sharp,
                            size: 30,
                            color: kPrimaryColor,
                          ),
                          title: Text(
                            'Location',
                            style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          subtitle: Text(
                            'Phnom Penh',
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),
                      const Divider(),
                      Card(
                        child: ListTile(
                          leading: Icon(
                            Icons.logout,
                            size: 30,
                            color: kPrimaryColor,
                          ),
                          title: Text(
                            'Logout',
                            style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),

                      // Card(
                      //   child: ListTile(
                      //     leading: FlutterLogo(),
                      //     title: Text('One-line with both widgets'),
                      //     trailing: Icon(Icons.more_vert),
                      //   ),
                      // ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      // ),
    );
  }

  // validationInput() {
  //   GetTextValue();
  // }
}




// Positioned(
//   top: 100,
//   left: 0,
//   child: Container(
//     height: 110.0,
//     width: 110.0,
//     padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
//     decoration:
//         BoxDecoration(shape: BoxShape.circle, color: Colors.white),
//     child: SvgPicture.asset(
//       "assets/icons/signup.svg",
//       width: size.width * 0.5,
//     ),
//   ),