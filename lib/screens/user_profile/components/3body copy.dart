// import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
// import 'package:login_project/components/rounded_button.dart';
import 'package:login_project/screens/user_profile/components/background.dart';

class Body extends StatelessWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // SizedBox(height: size.height * 0.03),
            // Padding(
            //   padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            //   child: Stack(
            //     fit: StackFit.loose,
            //     children: <Widget>[
            //       // Row(
            //       //   crossAxisAlignment: CrossAxisAlignment.center,
            //       //   mainAxisAlignment: MainAxisAlignment.center,
            //       //   children: <Widget>[
            //       Container(
            //         width: 140.0,
            //         height: 140.0,
            //         decoration: BoxDecoration(
            //           shape: BoxShape.circle,
            //           image: DecorationImage(
            //             image: const ExactAssetImage(
            //                 'assets/images/login_bottom.png'),
            //             fit: BoxFit.cover,
            //           ),
            //         ),
            //       ),
            //       // ],
            //       // )
            //     ],
            //   ),
            // Stack( children: <Widget>[
            //   Row(
            //       crossAxisAlignment: CrossAxisAlignment.center,
            //       mainAxisAlignment: MainAxisAlignment.center,
            //     children: <Widget>[
            //       new Container(
            //           width: 140.0,
            //           height: 140.0,
            //           decoration: new BoxDecoration(
            //             shape: BoxShape.circle,
            //             image: new DecorationImage(
            //               image: new ExactAssetImage(
            //                   'assets/images/as.png'),
            //               fit: BoxFit.cover,
            //             ),
            //           )),
            //     ],
            // ),
            Positioned(
              top: 0,
              left: 0,
              // left: size.width,

              child: Container(
                height: 110.0,
                width: 110.0,
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.white),
                child: SvgPicture.asset(
                  "assets/icons/signup.svg",
                  width: size.width * 0.5,
                ),
              ),

              // child: Image.asset(
              //   "assets/images/main_top.png",
              //   width: size.width * 0.35,
              // ),
            ),

            // Text(
            //   "Username",
            //   style: TextStyle(
            //     fontSize: 14,
            //     fontWeight: FontWeight.bold,
            //   ),
            // ),
            // SizedBox(height: size.height * 0.02),
          ],
        ),
      ),
    );
  }

  // validationInput() {
  //   GetTextValue();
  // }
}
