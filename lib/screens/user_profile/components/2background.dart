import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_project/constrant.dart';

class Background extends StatelessWidget {
  final Widget child;
  const Background({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              height: 200.0,
              color: kPrimaryLightColor,
              // child: Center(
              //   child: Text('Background image goes here'),
              // ),
            ),
            // Text('Background image goes here'),
          ],
        ),
        // Positioned(
        //     top: 140.0, // (background container size) - (circle height / 2)
        //     child: new Row(
        //       mainAxisAlignment: MainAxisAlignment.center,
        //       children: <Widget>[
        //         // TODO: this is the edit icon
        //         // Padding(
        //         //     padding: EdgeInsets.only(top: 0.0, right: 100.0),
        //         //     child: new Row(
        //         //       mainAxisAlignment: MainAxisAlignment.center,
        //         //       children: <Widget>[
        //         //         new CircleAvatar(
        //         //           backgroundColor: Colors.red,
        //         //           radius: 25.0,
        //         //           child: new Icon(
        //         //             Icons.camera_alt,
        //         //             color: Colors.white,
        //         //           ),
        //         //         )
        //         //       ],
        //         //     )),

        //         Container(
        //           height: 110.0,
        //           width: 110.0,
        //           decoration: BoxDecoration(
        //               shape: BoxShape.circle, color: Colors.white),
        //           child: SvgPicture.asset(
        //             "assets/icons/signup.svg",
        //             width: size.width * 0.5,
        //           ),
        //         ),

        //         // new CircleAvatar(
        //         //   backgroundColor: Colors.red,
        //         //   radius: 25.0,
        //         //   child: new Icon(
        //         //     Icons.camera_alt,
        //         //     color: Colors.white,
        //         //   ),
        //         // )
        //       ],
        //     )),
        // // Padding(
        //     padding: EdgeInsets.only(top: 0.0, right: 100.0),
        //     child: new Row(
        //       mainAxisAlignment: MainAxisAlignment.center,
        //       children: <Widget>[
        //         new CircleAvatar(
        //           backgroundColor: Colors.red,
        //           radius: 25.0,
        //           child: new Icon(
        //             Icons.camera_alt,
        //             color: Colors.white,
        //           ),
        //         )
        //       ],
        //     )),
        child,
      ],
    );
  }
}





  // Positioned(
  //         top: 140.0, // (background container size) - (circle height / 2)
  //         child: Container(
  //           height: 110.0,
  //           width: 110.0,
  //           decoration:
  //               BoxDecoration(shape: BoxShape.circle, color: Colors.white),
  //           child: SvgPicture.asset(
  //             "assets/icons/signup.svg",
  //             width: size.width * 0.5,
  //           ),
  //         ),
  //       ),


// return Container(
//       width: double.infinity,
//       height: size.height,
//       child: Stack(
//         alignment: Alignment.center,
//         children: <Widget>[
//           // Container(
//           //   height: 200.0,
//           //   color: Colors.orange,
//           //   child: Center(
//           //     child: Text('Background image goes here'),
//           //   ),
//           // ),
//           Positioned(
//             top: 0,
//             left: 0,
//             child: Image.asset(
//               "assets/images/main_top.png",
//               width: size.width * 0.35,
//             ),
//           ),
//           Positioned(
//             bottom: 0,
//             right: 0,
//             child: Image.asset(
//               "assets/images/login_bottom.png",
//               width: size.width * 0.4,
//             ),
//           ),
//           child,
//         ],
//       ),
//     );