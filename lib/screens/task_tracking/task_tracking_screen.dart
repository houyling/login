import 'package:flutter/material.dart';
import 'package:login_project/constrant.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:login_project/screens/home/home_screen.dart';
import 'package:login_project/screens/includes/app_nav.dart';
import 'package:login_project/screens/includes/bottom_nav.dart';
import 'package:login_project/screens/includes/menu.dart';
import 'package:login_project/screens/task_tracking/components/body.dart';

class TaskTrackingScreen extends StatefulWidget {
  const TaskTrackingScreen({Key? key}) : super(key: key);
  @override
  _TaskTrackingScreen createState() => _TaskTrackingScreen();
}

class _TaskTrackingScreen extends State<TaskTrackingScreen> {
  //State class
  int _page = 1;
  GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();

// This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    // Body();
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppNav(context, 1),
      bottomNavigationBar: BottomNav(context, _page, _bottomNavigationKey),
      body: Body(),
      // TODO:
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () => setState(() {
      //     _count++;
      //     print(_count);
      //   }),
      //   tooltip: 'Task',
      //   backgroundColor: kPrimaryColor,
      //   child: const Icon(
      //     Icons.add,
      //     color: Colors.white,
      //   ),
      // ),
    );

    //  Scaffold(
    //   appBar: AppBar(
    //     backgroundColor: kPrimaryColor,
    //     centerTitle: true, // set the title to center
    //     title: const Text('Task Tracking'),
    //     leading: IconButton(
    //       icon: Icon(Icons.keyboard_arrow_left_rounded, size: 30),
    //       onPressed: () {
    //         Navigator.push(
    //           context,
    //           MaterialPageRoute(
    //             builder: (context) => HomeScreen(),
    //           ), // navigate to the login screen
    //         );
    //       },
    //     ),
    //   ),
    //   body: Body(),
    //   // const Center(
    //   //   child: Text(
    //   //     'This is the next page',
    //   //     style: TextStyle(fontSize: 24),
    //   //   ),
    //   // ),
    // );

    // return Scaffold(
    //   body: Body(),
    // );
  }
}






// class NavigationBar extends StatefulWidget {
//   const NavigationBar({Key? key}) : super(key: key);

//   @override
//   _NavigationBarState createState() => _NavigationBarState();
// }

// class _NavigationBarState extends State<NavigationBar> {
//   //State class
//   int _page = 1;
//   GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();

//   @override
//   Widget build(BuildContext context) {
//     return CurvedNavigationBar(
//       // backgroundColor: Colors.blueAccent,
//       backgroundColor: kPrimaryColor,
//       items: <Widget>[
//         Icon(Icons.list, size: 30),
//         Icon(Icons.add, size: 30),
//         Icon(Icons.compare_arrows, size: 30),
//       ],
//       onTap: (index) {
//         //Handle button tap
//         print(index);
//         setState(() {
//           _page = index;
//         });
//         // setState(() {
//         //   _page = index;
//         // });
//       },
//     );
//   }
// }





// TODO: this is not working, need to fix 
// class GetTextValue extends StatefulWidget {
//   // const GetTextValue({Key? key}) : super(key: key);
//   @override
//   _GetTextValueState createState() => _GetTextValueState();
// }

// class _GetTextValueState extends State<GetTextValue> {
//   final emailInput = TextEditingController();
//   final passwordInput = TextEditingController();
//   String email = '';
//   String password = '';

//   getTextInputData() {
//     setState(() {
//       email = emailInput.text;
//       password = passwordInput.text;
//       print(email);
//       print(password);
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Body();
//   }
// }


// TODO: testing button
// Right Arrow to the Next page button
// IconButton(
//   icon: const Icon(Icons.navigate_next),
//   tooltip: 'Go to the next page',
//   onPressed: () {
//     Navigator.push(context, MaterialPageRoute<void>(
//       builder: (BuildContext context) {
//         return Scaffold(
//           appBar: AppBar(
//             backgroundColor: kPrimaryColor,
//             title: const Text('Next page'),
//           ),
//           body: const Center(
//             child: Text(
//               'This is the next page',
//               style: TextStyle(fontSize: 24),
//             ),
//           ),
//         );
//       },
//     ));
//   },
// ),