// import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_project/components/card_box.dart';
// import 'package:login_project/components/rounded_button.dart';
import 'package:login_project/constrant.dart';
import 'package:login_project/screens/task_tracking/components/background.dart';

class Body extends StatelessWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: Column(
        children: [
          const Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: Text(
              "PK 202112000001",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                letterSpacing: 1,
                color: kPrimaryColor,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          // TODO: will convert to the map box
          SizedBox(
            height: 200,
            child: Stack(children: <Widget>[
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: Card(
                  semanticContainer: true,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  child: SizedBox(
                    height: 185.0,
                    child: Image.asset(
                      'assets/images/delivery_man_banner.jpg',
                      // fit: BoxFit.cover,
                      fit: BoxFit.fill,
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 5,
                  margin: EdgeInsets.all(10),
                  color: Colors.transparent,
                  shadowColor: Colors.grey,
                ),
              ),
              Positioned(
                top: 22,
                left: 25,
                right: 0,
                child: Container(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text(
                          'Send Package to any where',
                          style: TextStyle(
                            fontSize: 12,
                            letterSpacing: 1.5,
                            height: 1.2,
                            color: Colors.blueGrey,
                            // fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 8),
                        Text(
                          'with Parcel Tracker.',
                          style: TextStyle(
                            fontSize: 12,
                            letterSpacing: 1.5,
                            height: 1.2,
                            color: kPrimaryColor,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ]),
          ),
          SizedBox(height: size.height * 0.02),

          Expanded(
            flex: 1,
            child: ListView(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              padding: EdgeInsets.all(10),
              children: const <Widget>[
                Text(
                  "Dec 18, 2021",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1,
                    color: Colors.black87,
                  ),
                  textAlign: TextAlign.left,
                ),
                // Card(child: ListTile(title: Text('One-line ListTile'))),
                CardBox(
                  titleText: 'PK 202112000001',
                  subtitleText: 'Jakarata - Sukabumi',
                  is_icon: false,
                  leadingImage: 'assets/icons/delivery_motor.svg',
                ),
                CardBox(
                  titleText: 'PK 202112000002',
                  subtitleText: 'Jakarata - Sukabumi',
                  trailingText: 'Deliveried',
                  trailingColor: kPrimaryColor,
                  is_icon: false,
                  leadingImage: 'assets/icons/delivery_done.svg',
                ),
                CardBox(
                  titleText: 'PK 202112000003',
                  subtitleText: 'Palembang - Sukabumi',
                  trailingText: 'Pending',
                  trailingColor: Colors.green,
                  is_icon: false,
                  leadingImage: 'assets/icons/delivery_pending.svg',
                ),
                CardBox(
                  titleText: 'PK 202112000004',
                  subtitleText: 'Selena - Gomez',
                  trailingText: 'Trainsmit',
                ),
                CardBox(
                  titleText: 'PK 202112000004',
                  subtitleText: 'Palembang - Sukabumi',
                  trailingText: 'Pending',
                  trailingColor: Colors.green,
                ),
              ],
            ),
          ),

          // SizedBox(height: size.height * 0.08),
        ],
      ),
    );
  }

  // validationInput() {
  //   GetTextValue();
  // }
}


// TODO::Parcel Tracking Screen
// https://dribbble.com/shots/10926860-Truck-Driver-app
// https://dribbble.com/shots/13140115-Build-an-On-demand-Courier-Delivery-App/attachments/4742114?mode=media