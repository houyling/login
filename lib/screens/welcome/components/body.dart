import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:login_project/constrant.dart';
import 'package:login_project/components/rounded_button.dart';
import 'package:login_project/screens/login/login_screen.dart';
import 'package:login_project/screens/signup/signup_screen.dart';
import 'package:login_project/screens/welcome/components/backround.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size provide us total height and width of our screen
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "WELCOME TO THE WORLD",
              style: TextStyle(
                // color: kPrimaryColor,
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: size.height * 0.05),
            SvgPicture.asset("assets/icons/chat.svg",
                height: size.height * 0.45),
            SizedBox(height: size.height * 0.02),
            RoundedButton(
              text: "LOGIN",
              onPress: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => LoginScreen(),
                  ), // navigate to the login screen
                ),
              },
            ),
            // Add button using ElevatedButton, this work
            // ElevatedButton(
            //   child: Text("LOGIN"),
            //   onPressed: () {
            //     Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //         builder: (context) => LoginScreen(),
            //       ),
            //     );
            //   },
            // ),
            RoundedButton(
              text: "SIGNUP",
              color: kPrimaryLightColor,
              textColor: Colors.black,
              onPress: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SignupScreen(),
                  ), // navigate to the login screen
                ),
              },
              // onPress: () => signupPress(context, LoginScreen()),
            ),
          ],
        ),
      ),
    );
  }
}

// This also work too
// RoundedButton(
//   text: "LOGIN",
//   onPress: () {
//     Navigator.push(
//       context,
//       MaterialPageRoute(builder: (BuildContext context) {
//         return LoginScreen();
//       }),
//     );
//   },
// ),
// Add button using ElevatedButton, this work
// ElevatedButton(
//   child: Text("LOGIN"),
//   onPressed: () {
//     Navigator.push(
//       context,
//       MaterialPageRoute(
//         builder: (context) => LoginScreen(),
//       ),
//     );
//   },
// ),