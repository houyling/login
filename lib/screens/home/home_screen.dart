import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:login_project/constrant.dart';
// import 'package:login_project/constrant.dart';
import 'package:login_project/screens/home/tabs/location_tab.dart';
import 'package:login_project/screens/home/tabs/track_tab.dart';
import 'package:login_project/screens/home/tabs/history_tab.dart';
import 'package:login_project/screens/includes/app_nav.dart';
import 'package:login_project/screens/includes/bottom_nav.dart';
import 'package:login_project/screens/includes/menu.dart';
// import 'package:login_project/screens/notification/notification_screen.dart';
// import 'package:login_project/screens/task/task_screen.dart';
// import 'package:login_project/screens/task_tracking/task_tracking_screen.dart';
// // import 'package:login_project/screens/home/components/body.dart';
// import 'package:login_project/screens/user_profile/user_profile_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  //State class
  int _page = 0;
  int _count = 0;
  GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();

// This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    var _count = 0;
    return DefaultTabController(
      initialIndex: 0,
      length: 3,
      child: Scaffold(
        drawer: NavDrawer(),
        appBar: AppNav(context, _page),
        bottomNavigationBar: BottomNav(context, _page, _bottomNavigationKey),
        body: _page == 0 ? HomeTabBar() : Container(),
        floatingActionButton: FloatingActionButton(
          // onPressed: () => setState(() {
          //   _count++;
          //   print(_count);
          // }),
          onPressed: () {
            setState(() {});
          },

          tooltip: 'Task',
          backgroundColor: kPrimaryColor,
          child: const Icon(
            Icons.add,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}

class HomeTabBar extends StatelessWidget {
  const HomeTabBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const TabBarView(
      children: <Widget>[
        LocationTabBar(),
        TrackTabBar(),
        HistoryTabBar(),
      ],
    );
  }
}




// #region TODO: Testing code backup

//  Navigator.push(context, MaterialPageRoute<void>(
//                 builder: (BuildContext context) {
//                   return Scaffold(
//                     appBar: AppBar(
//                       backgroundColor: kPrimaryColor,
//                       title: const Text('Next page'),
//                     ),
//                     body: const Center(
//                       child: Text(
//                         'This is the next page',
//                         style: TextStyle(fontSize: 24),
//                       ),
//                     ),
//                   );
//                 },
//               ));

// class NavigationBar extends StatefulWidget {
//   const NavigationBar({Key? key}) : super(key: key);

//   @override
//   _NavigationBarState createState() => _NavigationBarState();
// }

// class _NavigationBarState extends State<NavigationBar> {
//   //State class
//   int _page = 1;
//   GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();

//   @override
//   Widget build(BuildContext context) {
//     return CurvedNavigationBar(
//       // backgroundColor: Colors.blueAccent,
//       backgroundColor: kPrimaryColor,
//       items: <Widget>[
//         Icon(Icons.list, size: 30),
//         Icon(Icons.add, size: 30),
//         Icon(Icons.compare_arrows, size: 30),
//       ],
//       onTap: (index) {
//         //Handle button tap
//         print(index);
//         setState(() {
//           _page = index;
//         });
//         // setState(() {
//         //   _page = index;
//         // });
//       },
//     );
//   }
// }

// TODO: this is not working, need to fix
// class GetTextValue extends StatefulWidget {
//   // const GetTextValue({Key? key}) : super(key: key);
//   @override
//   _GetTextValueState createState() => _GetTextValueState();
// }

// class _GetTextValueState extends State<GetTextValue> {
//   final emailInput = TextEditingController();
//   final passwordInput = TextEditingController();
//   String email = '';
//   String password = '';

//   getTextInputData() {
//     setState(() {
//       email = emailInput.text;
//       password = passwordInput.text;
//       print(email);
//       print(password);
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Body();
//   }
// }

// TODO: testing button
// Right Arrow to the Next page button
// IconButton(
//   icon: const Icon(Icons.navigate_next),
//   tooltip: 'Go to the next page',
//   onPressed: () {
//     Navigator.push(context, MaterialPageRoute<void>(
//       builder: (BuildContext context) {
//         return Scaffold(
//           appBar: AppBar(
//             backgroundColor: kPrimaryColor,
//             title: const Text('Next page'),
//           ),
//           body: const Center(
//             child: Text(
//               'This is the next page',
//               style: TextStyle(fontSize: 24),
//             ),
//           ),
//         );
//       },
//     ));
//   },
// ),
// #endregion
