import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:login_project/constrant.dart';

class LocationTabBar extends StatelessWidget {
  const LocationTabBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    // return MapSample();
    return Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 200,
          child: Card(
            semanticContainer: true,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            child: MapSample(),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            elevation: 3,
            // margin: EdgeInsets.all(10),
            color: Colors.transparent,
            shadowColor: Colors.grey,
          ),
        ),
        SizedBox(height: size.height * 0.01),
        Expanded(
          flex: 1,
          child: ListView(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            padding: EdgeInsets.all(10),
            children: const <Widget>[
              Text(
                "Details",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1,
                  color: kPrimaryColor,
                ),
                textAlign: TextAlign.left,
              ),
              // Card(child: ListTile(title: Text('One-line ListTile'))),
            ],
          ),
        ),
      ],
    );
  }
}

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  // 1
  Completer<GoogleMapController> _controller = Completer();
  // 2
  static final CameraPosition _myLocation = CameraPosition(
    target: LatLng(11.5812183, 104.8843289),
    zoom: 12,
    bearing: 15.0, // 1
    tilt: 75.0, // 2
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // 1
      body: GoogleMap(
        // 2
        initialCameraPosition: _myLocation,
        // 3
        mapType: MapType.normal,
        // 4
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
    );
  }
}


    // return Container(
    //   alignment: Alignment.center,
    //   // height: size.height * 0.65,
    //   // width: size.width * 0.9,
    //   child: Center(
    //     child: Text("It's cloudy here"),
    //   ),
    // );

    