import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_project/components/rounded_input_field.dart';
import 'package:login_project/constrant.dart';

class Background extends StatelessWidget {
  final Widget child;
  const Background({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Stack(
          fit: StackFit.loose,
          alignment: Alignment.bottomCenter,
          overflow: Overflow.visible,
          children: <Widget>[
            Container(
              height: 150.0,
              color: kPrimaryLightColor,
              child: Center(
                child: Text('Background image goes here'),
              ),
            ),
            Positioned(
              top: 130,
              // left: 140,
              child: SizedBox(
                height: size.height,
                width: size.width,
                // width: size.width * 0.80,
                child: Card(
                  margin: EdgeInsets.all(10),
                  elevation: 5.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: size.height * 0.06),
                      Padding(
                        // padding: EdgeInsets.all(15.0),
                        padding: EdgeInsets.only(top: 25.0),
                        child: Text(
                          "Username",
                          style: TextStyle(
                            fontSize: 16.0,
                            // fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      // Padding(
                      //   padding:
                      //       EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                      //   child: Row(
                      //     mainAxisSize: MainAxisSize.max,
                      //     children: <Widget>[
                      //       SizedBox(height: size.height * 0.06),
                      //       Text(
                      //         "Email: ",
                      //         style: TextStyle(color: Colors.black),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      // SizedBox(height: size.height * 0.02),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              top: 90,
              child: Container(
                height: 110.0,
                width: 110.0,
                // padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.white),
                child: SvgPicture.asset(
                  "assets/icons/signup.svg",
                  width: size.width * 0.5,
                ),
              ),
            ),
            Positioned(
              right: 0,
              bottom: 0,
              child: Container(
                height: 35.0,
                width: 35.0,
                child: const CircleAvatar(
                  backgroundColor: Colors.red,
                  radius: 25.0,
                  child: Icon(
                    Icons.edit,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Positioned(
              top: 160,
              left: 140,
              child: Container(
                height: 35.0,
                width: 35.0,
                child: const CircleAvatar(
                  backgroundColor: Colors.red,
                  radius: 25.0,
                  child: Icon(
                    Icons.camera_alt,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
        Expanded(
          flex: 1,
          child: child,
        )

        // Column(
        //   children: [
        // Container(
        //   // width: double.infinity,
        //   alignment: Alignment.center,
        //   child: Center(
        //     // children: [
        //     child: child,
        //     // ],
        //   ),
        // ),
        //   ],
        // )
        // ),

        // SizedBox(height: size.height * 0.08),
      ],
    );
  }
}





        

// return Container(
//       width: double.infinity,
//       // height: size.height,
//       child: Stack(
//         fit: StackFit.loose,
//         alignment: Alignment.bottomCenter,
//         children: <Widget>[
//           Column(
//             children: [
//               Container(
//                 height: 150.0,
//                 color: kPrimaryLightColor,
//                 child: Center(
//                   child: Text('Background image goes here'),
//                 ),
//               ),
//               child,
//             ],
//           ),
//           // Positioned(
//           //   bottom: 0,
//           //   // right: 0,
//           //   child: child,
//           // ),
//         ],
//       ),
//     );

