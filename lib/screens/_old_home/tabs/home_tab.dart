import 'dart:async';

import 'package:flutter/material.dart';
// import 'package:login_project/components/card_box.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:login_project/constrant.dart';
import 'package:location/location.dart';
import 'package:login_project/constrant.dart';

class HomeTabBar extends StatelessWidget {
  const HomeTabBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    // return MapSample();
    return Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // const Padding(
        //   padding: EdgeInsets.only(top: 8.0),
        //   child: Text(
        //     "Tracking Package",
        //     style: TextStyle(
        //       fontSize: 18,
        //       fontWeight: FontWeight.bold,
        //       letterSpacing: 1,
        //       color: kPrimaryColor,
        //     ),
        //     textAlign: TextAlign.left,
        //   ),
        // ),
        SizedBox(
          height: 200,
          child: Card(
            semanticContainer: true,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            child: MapSample(),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            elevation: 3,
            // margin: EdgeInsets.all(10),
            color: Colors.transparent,
            shadowColor: Colors.grey,
          ),
        ),
        SizedBox(height: size.height * 0.01),
        Expanded(
          flex: 1,
          child: ListView(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            padding: EdgeInsets.all(10),
            children: const <Widget>[
              Text(
                "Recent Tracker",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1,
                  color: kPrimaryColor,
                ),
                textAlign: TextAlign.left,
              ),
              // Card(child: ListTile(title: Text('One-line ListTile'))),
            ],
          ),
        ),
      ],
    );
  }
}

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  // LatLng currentLocation = LatLng(11.5812183, 104.8843289);
  GoogleMapController? _controller;
  Location currentLocation = Location();
  final Set<Marker> _markers = {};

  // final Completer<GoogleMapController> _controller = Completer();

  void getLocation() async {
    var location = await currentLocation.getLocation();
    currentLocation.onLocationChanged.listen((LocationData loc) {
      _controller
          ?.animateCamera(CameraUpdate.newCameraPosition(new CameraPosition(
        target: LatLng(loc.latitude ?? 0.0, loc.longitude ?? 0.0),
        zoom: 12.0,
      )));
      // print(loc.latitude);
      // print(loc.longitude);
      setState(() {
        _markers.add(Marker(
            markerId: MarkerId('Home'),
            position: LatLng(loc.latitude ?? 0.0, loc.longitude ?? 0.0)));
      });
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      getLocation();
    });
  }

  // static final CameraPosition _kGooglePlex = CameraPosition(
  //   // target: LatLng(37.42796133580664, -122.085749655962),
  //   target: LatLng(11.5812183, 104.8843289),
  //   zoom: 14.4746,
  // );

  // TODO: Not use
  // static final CameraPosition _kLake = CameraPosition(
  //     bearing: 192.8334901395799,
  //     target: LatLng(37.43296265331129, -122.08832357078792),
  //     tilt: 59.440717697143555,
  //     zoom: 19.151926040649414);

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      mapType: MapType.normal,
      // liteModeEnabled: true,
      tiltGesturesEnabled: true,
      myLocationEnabled: true,
      initialCameraPosition: CameraPosition(
        target: LatLng(11.5813, 104.8847),
        // target: LatLng(0, 0),
        bearing: 192.8334901395799,
        zoom: 10.0,
      ),
      onMapCreated: (GoogleMapController controller) {
        _controller = controller;
      },
      markers: _markers,
      // floatingActionButton: FloatingActionButton(
      //   child: Icon(
      //     Icons.location_searching,
      //     color: Colors.white,
      //   ),
      //   onPressed: () {
      //     getLocation();
      //   },
      // ),
      // floatingActionButton: FloatingActionButton.extended(
      //     onPressed: getLocation,
      //     // label: Text('To the lake!'),
      //     icon: Icon(
      //       Icons.location_searching,
      //       color: Colors.white,
      //     )),
    );
  }

  // TODO: Not use
  // Future<void> getLocation() async {
  //   final GoogleMapController controller = await _controller.future;
  //   controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  // }
}


    // return Container(
    //   alignment: Alignment.center,
    //   // height: size.height * 0.65,
    //   // width: size.width * 0.9,
    //   child: Center(
    //     child: Text("It's cloudy here"),
    //   ),
    // );

    