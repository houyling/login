import 'package:flutter/material.dart';
import 'package:login_project/components/card_box.dart';
import 'package:login_project/components/rounded_input_field.dart';
import 'package:login_project/constrant.dart';

class HistoryTabBar extends StatelessWidget {
  const HistoryTabBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Card(child: ListTile(title: Text('One-line ListTile'))),
        const Padding(
          padding: EdgeInsets.only(top: 9.0),
          child: Text(
            "Tracking History",
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              letterSpacing: 1,
              color: kPrimaryColor,
            ),
            textAlign: TextAlign.left,
          ),
        ),
        SizedBox(
          height: 100,
          child: Stack(children: <Widget>[
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child: SizedBox(
                  height: size.height * 0.1,
                  // color: kPrimaryColor,
                  // child: Image.asset(
                  //   'assets/images/delivery_man_banner.jpg',
                  //   // fit: BoxFit.cover,
                  //   fit: BoxFit.fill,
                  // ),
                  child: RoundedInputField(
                    hintText: "Search",
                    icon: Icons.search,
                    // textInput:
                    //     emailController, // TODO: need to get the text from input
                    onChanged: (value) => {print(value)},
                  ),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                elevation: 5,
                margin: EdgeInsets.all(10),
                color: kPrimaryColor,
                // color: Colors.transparent,
                shadowColor: Colors.grey,
              ),
            ),
            // Positioned(
            //   top: 22,
            //   left: 25,
            //   right: 0,
            //   child: Container(
            //     alignment: Alignment.topLeft,
            //     child: Padding(
            //       padding: const EdgeInsets.all(8.0),
            //       child: Column(
            //         children: [
            //           Text(
            //             'Send Package to any where',
            //             style: TextStyle(
            //               fontSize: 12,
            //               letterSpacing: 1.5,
            //               height: 1.2,
            //               color: Colors.blueGrey,
            //               // fontWeight: FontWeight.bold,
            //             ),
            //             textAlign: TextAlign.center,
            //           ),
            //           SizedBox(height: 8),
            //           Text(
            //             'with Parcel Tracker.',
            //             style: TextStyle(
            //               fontSize: 12,
            //               letterSpacing: 1.5,
            //               height: 1.2,
            //               color: kPrimaryColor,
            //               fontWeight: FontWeight.bold,
            //             ),
            //             textAlign: TextAlign.left,
            //           ),
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
          ]),
        ),
        // SizedBox(height: size.height * 0.02),

        Expanded(
          flex: 1,
          child: ListView(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            padding: EdgeInsets.all(10),
            children: const <Widget>[
              Text(
                "History",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1,
                  color: kPrimaryColor,
                ),
                textAlign: TextAlign.left,
              ),
              // Card(child: ListTile(title: Text('One-line ListTile'))),
              Card(
                child: ListTile(
                  leading: Icon(
                    Icons.access_time_outlined,
                    size: 30,
                    color: Colors.grey,
                  ),
                  title: Text(
                    "PK 202112000001",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  trailing: Icon(Icons.close_outlined),
                ),
              ),
              Card(
                child: ListTile(
                  leading: Icon(
                    Icons.access_time_outlined,
                    size: 30,
                    color: Colors.grey,
                  ),
                  title: Text(
                    "PK 202112056468",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  trailing: Icon(Icons.close_outlined),
                ),
              ),
              Card(
                child: ListTile(
                  leading: Icon(
                    Icons.access_time_outlined,
                    size: 30,
                    color: Colors.grey,
                  ),
                  title: Text(
                    "PK 202112006411",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  trailing: Icon(Icons.close_outlined),
                ),
              ),

              Padding(
                padding: EdgeInsets.all(15.0),
                child: Text(
                  "Clear History",
                  style: TextStyle(
                    fontSize: 14,
                    // fontWeight: FontWeight.bold,
                    letterSpacing: 1,
                    color: Colors.grey,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),

        // SizedBox(height: size.height * 0.08),
      ],
    );
  }
}

// return Container(
//       // padding: EdgeInsets.only(top: 80.0),
//       alignment: Alignment.center,
//       // height: size.height * 0.65,
//       // width: size.width * 0.9,
//       child: Center(
//         child: Text("It's sunny here"),
//       ),
//     );
  