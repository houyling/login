import 'dart:async';

import 'package:flutter/material.dart';
import 'package:login_project/components/card_box.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:login_project/constrant.dart';

class HomeTabBar extends StatelessWidget {
  const HomeTabBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return MapSample();
    // return Column(
    //   // mainAxisAlignment: MainAxisAlignment.center,
    //   children: [
    //     const Padding(
    //       padding: EdgeInsets.only(top: 8.0),
    //       child: Text(
    //         "Tracking Package",
    //         style: TextStyle(
    //           fontSize: 18,
    //           fontWeight: FontWeight.bold,
    //           letterSpacing: 1,
    //           color: kPrimaryColor,
    //         ),
    //         textAlign: TextAlign.left,
    //       ),
    //     ),
    //     SizedBox(
    //       height: 200,
    //       child: Stack(children: <Widget>[
    //         Positioned(
    //           top: 0,
    //           left: 0,
    //           right: 0,
    //           child: Card(
    //             semanticContainer: true,
    //             clipBehavior: Clip.antiAliasWithSaveLayer,
    //             child: MapBox(),
    //             shape: RoundedRectangleBorder(
    //               borderRadius: BorderRadius.circular(15.0),
    //             ),
    //             elevation: 5,
    //             margin: EdgeInsets.all(10),
    //             color: Colors.transparent,
    //             shadowColor: Colors.grey,
    //           ),
    //         ),
    //         Positioned(
    //           top: 22,
    //           left: 25,
    //           right: 0,
    //           child: Container(
    //             alignment: Alignment.topLeft,
    //             child: Padding(
    //               padding: const EdgeInsets.all(8.0),
    //               child: Column(
    //                 children: [
    //                   Text(
    //                     'Send Package to any where',
    //                     style: TextStyle(
    //                       fontSize: 12,
    //                       letterSpacing: 1.5,
    //                       height: 1.2,
    //                       color: Colors.blueGrey,
    //                       // fontWeight: FontWeight.bold,
    //                     ),
    //                     textAlign: TextAlign.center,
    //                   ),
    //                   SizedBox(height: 8),
    //                   Text(
    //                     'with Parcel Tracker.',
    //                     style: TextStyle(
    //                       fontSize: 12,
    //                       letterSpacing: 1.5,
    //                       height: 1.2,
    //                       color: kPrimaryColor,
    //                       fontWeight: FontWeight.bold,
    //                     ),
    //                     textAlign: TextAlign.left,
    //                   ),
    //                 ],
    //               ),
    //             ),
    //           ),
    //         ),
    //       ]),
    //     ),
    //     SizedBox(height: size.height * 0.02),
    //     Expanded(
    //       flex: 1,
    //       child: ListView(
    //         scrollDirection: Axis.vertical,
    //         shrinkWrap: true,
    //         padding: EdgeInsets.all(10),
    //         children: const <Widget>[
    //           Text(
    //             "Recent Tracker",
    //             style: TextStyle(
    //               fontSize: 18,
    //               fontWeight: FontWeight.bold,
    //               letterSpacing: 1,
    //               color: kPrimaryColor,
    //             ),
    //             textAlign: TextAlign.left,
    //           ),
    //           // Card(child: ListTile(title: Text('One-line ListTile'))),
    //           CardBox(
    //             titleText: 'PK 202112000001',
    //             subtitleText: 'Jakarata - Sukabumi',
    //             is_icon: false,
    //             leadingImage: 'assets/icons/delivery_motor.svg',
    //           ),
    //           CardBox(
    //             titleText: 'PK 202112000002',
    //             subtitleText: 'Jakarata - Sukabumi',
    //             trailingText: 'Deliveried',
    //             trailingColor: kPrimaryColor,
    //             is_icon: false,
    //             leadingImage: 'assets/icons/delivery_done.svg',
    //           ),
    //           CardBox(
    //             titleText: 'PK 202112000003',
    //             subtitleText: 'Palembang - Sukabumi',
    //             trailingText: 'Pending',
    //             trailingColor: Colors.green,
    //             is_icon: false,
    //             leadingImage: 'assets/icons/delivery_pending.svg',
    //           ),
    //           CardBox(
    //             titleText: 'PK 202112000004',
    //             subtitleText: 'Selena - Gomez',
    //             trailingText: 'Trainsmit',
    //           ),
    //           CardBox(
    //             titleText: 'PK 202112000004',
    //             subtitleText: 'Palembang - Sukabumi',
    //             trailingText: 'Pending',
    //             trailingColor: Colors.green,
    //           ),
    //         ],
    //       ),
    //     ),
    //   ],
    // );
  }
}

class MapBox extends StatelessWidget {
  const MapBox({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 185.0,
      child: Image.asset(
        'assets/images/delivery_man_banner.jpg',
        // fit: BoxFit.cover,
        fit: BoxFit.fill,
      ),
    );
  }
}

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  final Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: GoogleMap(
        mapType: MapType.hybrid,
        initialCameraPosition: _kGooglePlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _goToTheLake,
        label: Text('To the lake!'),
        icon: Icon(Icons.directions_boat),
      ),
    );
  }

  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }
}


    // return Container(
    //   alignment: Alignment.center,
    //   // height: size.height * 0.65,
    //   // width: size.width * 0.9,
    //   child: Center(
    //     child: Text("It's cloudy here"),
    //   ),
    // );

    