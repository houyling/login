import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_project/components/card_box.dart';
import 'package:login_project/constrant.dart';

class TrackTabBar extends StatelessWidget {
  const TrackTabBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: Text(
            "App Banner",
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              letterSpacing: 1,
              color: kPrimaryColor,
            ),
            textAlign: TextAlign.left,
          ),
        ),
        SizedBox(
          height: 200,
          child: Stack(children: <Widget>[
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child: SizedBox(
                  height: 185.0,
                  child: Image.asset(
                    'assets/images/delivery_man_banner.jpg',
                    // fit: BoxFit.cover,
                    fit: BoxFit.fill,
                  ),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 3,
                margin: EdgeInsets.all(10),
                color: Colors.transparent,
                shadowColor: Colors.grey,
              ),
            ),
            Positioned(
              top: 22,
              left: 25,
              right: 0,
              child: Container(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Text(
                        'Send Package to any where',
                        style: TextStyle(
                          fontSize: 12,
                          letterSpacing: 1.5,
                          height: 1.2,
                          color: Colors.blueGrey,
                          // fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 8),
                      Text(
                        'with Parcel Tracker.',
                        style: TextStyle(
                          fontSize: 12,
                          letterSpacing: 1.5,
                          height: 1.2,
                          color: kPrimaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ]),
        ),
        SizedBox(height: size.height * 0.01),

        Expanded(
          flex: 1,
          child: ListView(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            padding: EdgeInsets.all(8),
            children: const <Widget>[
              Text(
                "Recent Tracker",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 0.8,
                  color: kPrimaryColor,
                ),
                textAlign: TextAlign.left,
              ),
              // Card(child: ListTile(title: Text('One-line ListTile'))),
              CardBox(
                titleText: 'PK 202112000001',
                subtitleText: 'Jakarata - Sukabumi',
                is_icon: false,
                leadingImage: 'assets/icons/delivery_motor.svg',
              ),
              CardBox(
                titleText: 'PK 202112000002',
                subtitleText: 'Jakarata - Sukabumi',
                trailingText: 'Deliveried',
                trailingColor: kPrimaryColor,
                is_icon: false,
                leadingImage: 'assets/icons/delivery_done.svg',
              ),
              CardBox(
                titleText: 'PK 202112000003',
                subtitleText: 'Palembang - Sukabumi',
                trailingText: 'Pending',
                trailingColor: Colors.green,
                is_icon: false,
                leadingImage: 'assets/icons/delivery_pending.svg',
              ),
              CardBox(
                titleText: 'PK 202112000004',
                subtitleText: 'Selena - Gomez',
                trailingText: 'Trainsmit',
              ),
              CardBox(
                titleText: 'PK 202112000004',
                subtitleText: 'Palembang - Sukabumi',
                trailingText: 'Pending',
                trailingColor: Colors.green,
              ),
            ],
          ),
        ),

        // SizedBox(height: size.height * 0.08),
      ],
    );
  }
}






// Stack(
// fit: StackFit.loose,
// alignment: Alignment.bottomCenter,
// overflow: Overflow.visible,
// children: <Widget>[
// Container(
//   height: 200.0,
//   // color: kPrimaryLightColor,
//   child: ListView(
//     // scrollDirection: Axis.vertical,
//     shrinkWrap: true,
//     children: const <Widget>[
//       // Card(child: ListTile(title: Text('One-line ListTile'))),
//       Text(
//         "User Information",
//         style: TextStyle(
//           fontSize: 14,
//           fontWeight: FontWeight.bold,
//         ),
//       ),
//       Card(
//         child: ListTile(
//           leading: Icon(
//             Icons.person,
//             size: 30,
//             color: kPrimaryColor,
//           ),
//           title: Text(
//             'Name',
//             style: TextStyle(
//               fontSize: 13,
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//           subtitle: Text(
//             'Selena Gomez',
//             style: TextStyle(
//               fontSize: 14,
//             ),
//           ),
//         ),
//       ),
//       Card(
//         child: ListTile(
//           leading: Icon(
//             Icons.phone,
//             size: 30,
//             color: kPrimaryColor,
//           ),
//           title: Text(
//             'Phone',
//             style: TextStyle(
//               fontSize: 13,
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//           subtitle: Text(
//             '012 123 456 / 011 123 456',
//             style: TextStyle(
//               fontSize: 14,
//             ),
//           ),
//         ),
//       ),
//       Card(
//         child: ListTile(
//           leading: Icon(
//             Icons.email,
//             size: 30,
//             color: kPrimaryColor,
//           ),
//           title: Text(
//             'Email',
//             style: TextStyle(
//               fontSize: 13,
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//           subtitle: Text(
//             'selena.gomez@gmail.com',
//             style: TextStyle(
//               fontSize: 14,
//             ),
//           ),
//         ),
//       ),
//       Card(
//         child: ListTile(
//           leading: Icon(
//             Icons.location_on_sharp,
//             size: 30,
//             color: kPrimaryColor,
//           ),
//           title: Text(
//             'Location',
//             style: TextStyle(
//               fontSize: 13,
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//           subtitle: Text(
//             'Phnom Penh',
//             style: TextStyle(
//               fontSize: 14,
//             ),
//           ),
//         ),
//       ),
//       const Divider(),
//       Card(
//         child: ListTile(
//           leading: Icon(
//             Icons.logout,
//             size: 30,
//             color: kPrimaryColor,
//           ),
//           title: Text(
//             'Logout',
//             style: TextStyle(
//               fontSize: 13,
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//         ),
//       ),

//       // Card(
//       //   child: ListTile(
//       //     leading: FlutterLogo(),
//       //     title: Text('One-line with both widgets'),
//       //     trailing: Icon(Icons.more_vert),
//       //   ),
//       // ),
//     ],
//   ),
// ),
// // Positioned(
// //   right: 0,
// //   bottom: 0,
// //   child: Container(
// //     height: 35.0,
// //     width: 35.0,
// //     child: const CircleAvatar(
// //       backgroundColor: Colors.red,
// //       radius: 25.0,
// //       child: Icon(
// //         Icons.edit,
// //         color: Colors.white,
// //       ),
// //     ),
// //   ),
// // ),
// // Positioned(
// //   top: 160,
// //   left: 140,
// //   child: Container(
// //     height: 35.0,
// //     width: 35.0,
// //     child: const CircleAvatar(
// //       backgroundColor: Colors.red,
// //       radius: 25.0,
// //       child: Icon(
// //         Icons.camera_alt,
// //         color: Colors.white,
// //       ),
// //     ),
// //   ),
// // ),
// ],
// ),




  //   return Container(
  //     // padding: EdgeInsets.only(top: 80.0),
  //     alignment: Alignment.center,
  //     // height: size.height * 0.65,
  //     // width: size.width * 0.9,
  //     child: Center(
  //       child: Text("It's rainy here"),
  //     ),
  //   );
  // }