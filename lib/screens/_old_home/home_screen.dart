import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:login_project/constrant.dart';
import 'package:login_project/screens/home/tabs/location_tab.dart';
import 'package:login_project/screens/home/tabs/track_tab.dart';
import 'package:login_project/screens/home/tabs/history_tab.dart';
// import 'package:login_project/screens/notification/notification_screen.dart';
import 'package:login_project/screens/task/task_screen.dart';
import 'package:login_project/screens/task_tracking/task_tracking_screen.dart';
// import 'package:login_project/screens/home/components/body.dart';
import 'package:login_project/screens/user_profile/user_profile_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  //State class
  int _page = 0;
  GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();

// This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return DefaultTabController(
      initialIndex: 0,
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: kPrimaryColor,
          automaticallyImplyLeading: false, // remove the leading icon
          centerTitle: true, // set the title to center
          title: const Text(
            'Parcel Tracking',
            style: TextStyle(letterSpacing: 0.5),
          ),
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.notifications, size: 25),
              tooltip: 'Show Snackbar',
              onPressed: () {
                ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('This is a snackbar')));
                // Navigator.push(
                //   context,
                //   MaterialPageRoute<void>(
                //     builder: (BuildContext context) =>
                //         const NotificationScreen(),
                //   ),
                // );
              },
            ),
            IconButton(
              icon: const Icon(Icons.account_circle, size: 25),
              tooltip: 'Show Snackbar',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute<void>(
                    builder: (BuildContext context) =>
                        const UserProfileScreen(),
                  ),
                );
              },
            ),
          ],
          bottom: const TabBar(
            tabs: <Widget>[
              Tab(
                // icon: Icon(Icons.map_rounded, size: 25),
                icon: Icon(Icons.pin_drop, size: 25),
              ),
              Tab(
                icon: Icon(Icons.delivery_dining, size: 25),
              ),
              Tab(
                icon: Icon(Icons.history, size: 25),
              ),
            ],
          ),
        ),
        bottomNavigationBar: CurvedNavigationBar(
          key: _bottomNavigationKey,
          backgroundColor: kPrimaryColor,
          index: 0,
          height: 50.0,
          items: <Widget>[
            Column(
              children: const [
                Icon(
                  Icons.home,
                  size: 20,
                  color: kPrimaryColor,
                ),
                Text(
                  'Home',
                  style: TextStyle(fontSize: 12),
                  // style: TextStyle(letterSpacing: 1.0),
                ),
              ],
            ),
            Column(
              children: const [
                Icon(
                  Icons.earbuds,
                  // Icons.pin_drop,
                  size: 20,
                  color: kPrimaryColor,
                ),
                Text(
                  'Pick up',
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
            Column(
              children: const [
                Icon(
                  Icons.inventory_rounded,
                  // Icons.inventory_rounded,
                  // Icons.receipt_outlined,
                  size: 20,
                  color: kPrimaryColor,
                ),
                Text(
                  'Order',
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
            Column(
              children: const [
                Icon(
                  Icons.menu,
                  size: 20,
                  color: kPrimaryColor,
                ),
                Text(
                  'Menu',
                  style: TextStyle(fontSize: 12),
                  // style: TextStyle(letterSpacing: 1.0),
                ),
              ],
            ),
          ],
          onTap: (index) {
            //Handle button tap
            setState(() {
              _page = index;
            });
            navigateScreens(_page, context);
          },
          letIndexChange: (index) => true,
        ),
        body: const TabBarView(
          children: <Widget>[
            LocationTabBar(),
            TrackTabBar(),
            HistoryTabBar(),
          ],
        ),
      ),
    );
  }

  navigateScreens(index, context) => navigateScreen(index, context);
}

// #region Bottom Navigation Screen
navigateScreen(index, context) {
  print(index);
  print('Hello World');
  switch (index) {
    case 0: // Home
      // this is the homw screen

      break;
    case 1: // Tracking
      Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => const TaskTrackingScreen(),
        ),
      );
      break;
    case 2: // Order
      Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => const TaskScreen(),
        ),
      );
      break;

    case 3: // Setting
      Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => const UserProfileScreen(),
        ),
      );
      break;

    default:
  }
}
// #endregion Bottom Navigation Screen









// #region TODO: Testing code backup

//  Navigator.push(context, MaterialPageRoute<void>(
//                 builder: (BuildContext context) {
//                   return Scaffold(
//                     appBar: AppBar(
//                       backgroundColor: kPrimaryColor,
//                       title: const Text('Next page'),
//                     ),
//                     body: const Center(
//                       child: Text(
//                         'This is the next page',
//                         style: TextStyle(fontSize: 24),
//                       ),
//                     ),
//                   );
//                 },
//               ));


// class NavigationBar extends StatefulWidget {
//   const NavigationBar({Key? key}) : super(key: key);

//   @override
//   _NavigationBarState createState() => _NavigationBarState();
// }

// class _NavigationBarState extends State<NavigationBar> {
//   //State class
//   int _page = 1;
//   GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();

//   @override
//   Widget build(BuildContext context) {
//     return CurvedNavigationBar(
//       // backgroundColor: Colors.blueAccent,
//       backgroundColor: kPrimaryColor,
//       items: <Widget>[
//         Icon(Icons.list, size: 30),
//         Icon(Icons.add, size: 30),
//         Icon(Icons.compare_arrows, size: 30),
//       ],
//       onTap: (index) {
//         //Handle button tap
//         print(index);
//         setState(() {
//           _page = index;
//         });
//         // setState(() {
//         //   _page = index;
//         // });
//       },
//     );
//   }
// }





// TODO: this is not working, need to fix 
// class GetTextValue extends StatefulWidget {
//   // const GetTextValue({Key? key}) : super(key: key);
//   @override
//   _GetTextValueState createState() => _GetTextValueState();
// }

// class _GetTextValueState extends State<GetTextValue> {
//   final emailInput = TextEditingController();
//   final passwordInput = TextEditingController();
//   String email = '';
//   String password = '';

//   getTextInputData() {
//     setState(() {
//       email = emailInput.text;
//       password = passwordInput.text;
//       print(email);
//       print(password);
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Body();
//   }
// }


// TODO: testing button
// Right Arrow to the Next page button
// IconButton(
//   icon: const Icon(Icons.navigate_next),
//   tooltip: 'Go to the next page',
//   onPressed: () {
//     Navigator.push(context, MaterialPageRoute<void>(
//       builder: (BuildContext context) {
//         return Scaffold(
//           appBar: AppBar(
//             backgroundColor: kPrimaryColor,
//             title: const Text('Next page'),
//           ),
//           body: const Center(
//             child: Text(
//               'This is the next page',
//               style: TextStyle(fontSize: 24),
//             ),
//           ),
//         );
//       },
//     ));
//   },
// ),
// #endregion