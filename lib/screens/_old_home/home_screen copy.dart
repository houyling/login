import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:login_project/constrant.dart';
import 'package:login_project/screens/task/_task_screen.dart';
import 'package:login_project/screens/home/components/body.dart';
import 'package:login_project/screens/signup/signup_screen.dart';
import 'package:login_project/screens/user_profile/user_profile_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  //State class
  int _page = 0;
  GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();

// This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        automaticallyImplyLeading: false, // remove the leading icon
        title: Row(
          children: [
            IconButton(
              icon: const Icon(Icons.home, size: 30),
              tooltip: 'Show Snackbar',
              onPressed: () {
                // notification flash
                // ScaffoldMessenger.of(context).showSnackBar(
                //     const SnackBar(content: Text('This is a snackbar')));
              },
            ),
            Text(
              'Home',
              style: TextStyle(
                fontSize: 22,
              ),
            ),
          ],
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.notifications, size: 30),
            tooltip: 'Show Snackbar',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('This is a snackbar')));
            },
          ),
        ],
      ),
      bottomNavigationBar: CurvedNavigationBar(
        key: _bottomNavigationKey,
        backgroundColor: kPrimaryColor,
        index: 1,
        height: 60.0,
        items: <Widget>[
          Icon(Icons.menu, size: 30),
          Icon(Icons.add, size: 30),
          Icon(Icons.person, size: 30),
        ],
        onTap: (index) {
          //Handle button tap
          setState(() {
            _page = index;
          });
          navigateScreens(_page, context);
        },
        letIndexChange: (index) => true,
      ),
      // body: Body(),
      // body: Container(
      //   color: kPrimaryLightColor,
      //   child: Center(
      //     child: Column(
      //       children: <Widget>[
      //         // Text(_page.toString(), textScaleFactor: 10.0),
      //         ElevatedButton(
      //           // child: Text('Go To Page of index 1'),
      //           child: Text(_page.toString(), textScaleFactor: 10.0),

      //           onPressed: () {
      //             //Page change using state does the same as clicking index 1 navigation button
      //             final CurvedNavigationBarState? navBarState =
      //                 _bottomNavigationKey.currentState;
      //             navBarState?.setPage(1);
      //             // print(_bottomNavigationKey.currentState);
      //             // print(navBarState);
      //           },
      //         )
      //       ],
      //     ),
      //   ),
      // ),
    );
  }

  navigateScreens(index, context) => navigateScreen(index, context);
}

navigateScreen(index, context) {
  print(index);
  print('Hello World');
  switch (index) {
    case 0:
      // this is the homw screen

      break;
    case 1: // TaskScreen
      Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => const TaskScreen(),
        ),
      );
      break;
    case 2: // TaskScreen
      Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => const UserProfileScreen(),
        ),
      );
      break;

    default:
  }
}

//  Navigator.push(context, MaterialPageRoute<void>(
//                 builder: (BuildContext context) {
//                   return Scaffold(
//                     appBar: AppBar(
//                       backgroundColor: kPrimaryColor,
//                       title: const Text('Next page'),
//                     ),
//                     body: const Center(
//                       child: Text(
//                         'This is the next page',
//                         style: TextStyle(fontSize: 24),
//                       ),
//                     ),
//                   );
//                 },
//               ));





// class NavigationBar extends StatefulWidget {
//   const NavigationBar({Key? key}) : super(key: key);

//   @override
//   _NavigationBarState createState() => _NavigationBarState();
// }

// class _NavigationBarState extends State<NavigationBar> {
//   //State class
//   int _page = 1;
//   GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();

//   @override
//   Widget build(BuildContext context) {
//     return CurvedNavigationBar(
//       // backgroundColor: Colors.blueAccent,
//       backgroundColor: kPrimaryColor,
//       items: <Widget>[
//         Icon(Icons.list, size: 30),
//         Icon(Icons.add, size: 30),
//         Icon(Icons.compare_arrows, size: 30),
//       ],
//       onTap: (index) {
//         //Handle button tap
//         print(index);
//         setState(() {
//           _page = index;
//         });
//         // setState(() {
//         //   _page = index;
//         // });
//       },
//     );
//   }
// }





// TODO: this is not working, need to fix 
// class GetTextValue extends StatefulWidget {
//   // const GetTextValue({Key? key}) : super(key: key);
//   @override
//   _GetTextValueState createState() => _GetTextValueState();
// }

// class _GetTextValueState extends State<GetTextValue> {
//   final emailInput = TextEditingController();
//   final passwordInput = TextEditingController();
//   String email = '';
//   String password = '';

//   getTextInputData() {
//     setState(() {
//       email = emailInput.text;
//       password = passwordInput.text;
//       print(email);
//       print(password);
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Body();
//   }
// }


// TODO: testing button
// Right Arrow to the Next page button
// IconButton(
//   icon: const Icon(Icons.navigate_next),
//   tooltip: 'Go to the next page',
//   onPressed: () {
//     Navigator.push(context, MaterialPageRoute<void>(
//       builder: (BuildContext context) {
//         return Scaffold(
//           appBar: AppBar(
//             backgroundColor: kPrimaryColor,
//             title: const Text('Next page'),
//           ),
//           body: const Center(
//             child: Text(
//               'This is the next page',
//               style: TextStyle(fontSize: 24),
//             ),
//           ),
//         );
//       },
//     ));
//   },
// ),