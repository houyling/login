import 'package:flutter/material.dart';
import 'package:login_project/constrant.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:login_project/screens/home/home_screen.dart';

import 'package:login_project/screens/task/task_screen.dart';
import 'package:login_project/screens/task_tracking/task_tracking_screen.dart';
import 'package:login_project/screens/user_profile/user_profile_screen.dart';

CurvedNavigationBar BottomNav(
    BuildContext context, _page, _bottomNavigationKey) {
  return CurvedNavigationBar(
    key: _bottomNavigationKey,
    backgroundColor: kPrimaryColor,
    index: _page,
    height: 50.0,
    items: <Widget>[
      Column(
        children: const [
          Icon(
            Icons.home,
            size: 25,
            color: kPrimaryColor,
          ),
          Text(
            'Home',
            style: TextStyle(fontSize: 12),
            // style: TextStyle(letterSpacing: 1.0),
          ),
        ],
      ),
      Column(
        children: const [
          Icon(
            Icons.earbuds,
            // Icons.pin_drop,
            size: 25,
            color: kPrimaryColor,
          ),
          Text(
            'Pick up',
            style: TextStyle(fontSize: 12),
          ),
        ],
      ),
      Column(
        children: const [
          Icon(
            Icons.inventory_rounded,
            // Icons.inventory_rounded,
            // Icons.receipt_outlined,
            size: 25,
            color: kPrimaryColor,
          ),
          Text(
            'Order',
            style: TextStyle(fontSize: 12),
          ),
        ],
      ),
      Column(
        children: const [
          Icon(
            Icons.message,
            size: 25,
            color: kPrimaryColor,
          ),
          Text(
            'Message',
            style: TextStyle(fontSize: 12),
            // style: TextStyle(letterSpacing: 1.0),
          ),
        ],
      ),
    ],
    onTap: (index) {
      //Handle button tap
      // setState(() {
      _page = index;
      // });
      navigateScreens(_page, context);
    },
    letIndexChange: (index) => true,
  );
}

void setState(Null Function() param0) {}

navigateScreens(index, context) => navigateScreen(index, context);

// #region Bottom Navigation Screen
navigateScreen(index, context) {
  print(index);
  print('Hello World');
  switch (index) {
    case 0: // Home
      // this is the homw screen
      Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => HomeScreen(),
        ),
      );
      break;
    case 1: // Tracking
      Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => TaskTrackingScreen(),
        ),
      );
      break;
    case 2: // Order
      Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => TaskScreen(),
        ),
      );
      break;

    case 3: // Setting
      Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => UserProfileScreen(),
        ),
      );
      break;

    default:
  }
}
// #endregion Bottom Navigation Screen

