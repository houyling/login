import 'package:flutter/material.dart';
import 'package:login_project/constrant.dart';
import 'package:login_project/screens/user_profile/user_profile_screen.dart';

AppBar AppNav(BuildContext context, _page) {
  print(_page);
  return AppBar(
    backgroundColor: kPrimaryColor,
    // automaticallyImplyLeading: false, // remove the leading icon
    toolbarHeight: 50,
    centerTitle: true, // set the title to center
    title: const Text(
      'Parcel Tracking',
      // style: TextStyle(letterSpacing: 0.5),
    ),
    // leading: IconButton(
    //   icon: const Icon(Icons.notes, size: 25),
    //   tooltip: 'Show Snackbar',
    //   onPressed: () {
    //     // Navigator.of(context).pop();
    //     // Navigator.push(
    //     //   context,
    //     //   MaterialPageRoute<void>(
    //     //     builder: (BuildContext context) => const UserProfileScreen(),
    //     //   ),
    //     // );
    //   },
    // ),
    actions: <Widget>[
      IconButton(
        icon: const Icon(Icons.notifications, size: 25),
        tooltip: 'Show Snackbar',
        onPressed: () {
          ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('This is a snackbar')));
          // Navigator.push(
          //   context,
          //   MaterialPageRoute<void>(
          //     builder: (BuildContext context) =>
          //         const NotificationScreen(),
          //   ),
          // );
        },
      ),
      // TODO: this will move to the side manu bar
      // IconButton(
      //   // icon: const Icon(Icons.account_circle, size: 25),
      //   icon: const Icon(Icons.language_outlined, size: 25),
      //   tooltip: 'Show Snackbar',
      //   onPressed: () {
      //     Navigator.push(
      //       context,
      //       MaterialPageRoute<void>(
      //         builder: (BuildContext context) => const UserProfileScreen(),
      //       ),
      //     );
      //   },
      // ),
    ],
    bottom: _page == 0 ? homeTab() : null,
  );
}

TabBar homeTab() {
  return const TabBar(
    tabs: <Widget>[
      Tab(
        // icon: Icon(Icons.map_rounded, size: 25),
        icon: Icon(Icons.pin_drop, size: 25),
      ),
      Tab(
        icon: Icon(Icons.delivery_dining, size: 25),
      ),
      Tab(
        icon: Icon(Icons.history, size: 25),
      ),
    ],
  );
}
