import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_project/constrant.dart';
import 'package:login_project/screens/login/login_screen.dart';
import 'package:login_project/screens/user_profile/user_profile_screen.dart';
import 'package:login_project/screens/welcome/welcome_screen.dart';

class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'User Profile',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                  textAlign: TextAlign.left,
                ),
                Container(
                  height: 90.0,
                  width: 90.0,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.only(top: 10),
                  child: const CircleAvatar(
                    backgroundColor: Colors.red,
                    radius: 25.0,
                    child: Icon(
                      Icons.person,
                      color: Colors.white,
                      size: 60,
                    ),
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
              // shape: BoxShape.circle,
              color: kPrimaryColor,
              //   image: DecorationImage(
              //     image: SvgPicture.asset("assets/icons/signup.svg")
              //   ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.input),
            title: Text('Welcome'),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => WelcomeScreen(),
                ), // navigate to the login screen
              ),
            },
          ),
          ListTile(
            leading: Icon(Icons.verified_user),
            title: Text('Profile'),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => UserProfileScreen(),
                ), // navigate to the login screen
              ),
            },
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text('Settings'),
            onTap: () => {
              Navigator.of(context).pop()
            }, // TODO: need to have a setting screen
          ),
          ListTile(
            leading: Icon(Icons.border_color),
            title: Text('Feedback'),
            onTap: () => {Navigator.of(context).pop()}, // TODO: testing screen
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Logout'),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => LoginScreen(),
                ), // navigate to the login screen
              ),
            },
          ),
        ],
      ),
    );
  }
}
