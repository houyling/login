import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_project/components/already_have_an_account.dart';
import 'package:login_project/components/rounded_button.dart';
import 'package:login_project/components/rounded_input_field.dart';
import 'package:login_project/components/rounded_password_field.dart';
import 'package:login_project/screens/login/components/background.dart';
import 'package:login_project/screens/signup/signup_screen.dart';
import 'package:login_project/screens/home/home_screen.dart';

class Body extends StatelessWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController emailController = new TextEditingController();
    String email;

    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "LOGIN",
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
                height: size.height * 0.03), // add margin after the text above
            SvgPicture.asset(
              "assets/icons/login.svg",
              height: size.height * 0.35,
            ),
            SizedBox(
                height: size.height * 0.02), // add margin after the text above
            RoundedInputField(
              hintText: "Email",
              icon: Icons.person_outline_rounded,
              // textInput:
              //     emailController, // TODO: need to get the text from input
              onChanged: (value) {
                print(value);
              },
            ),
            // TODO: Testing only
            // TextField(
            //   onChanged: (value) => {email = value},
            //   controller:
            //       emailController, // to get the text from the user input
            //   decoration: InputDecoration(
            //     icon: Icon(
            //       Icons.person,
            //       color: Colors.amber,
            //     ),
            //     hintText: "Test Input",
            //     border: InputBorder.none,
            //   ),
            // ),

            RoundedPasswordField(
              onChanged: (value) => {},
            ),
            RoundedButton(
              text: "Login",
              onPress: () => {
                // print('Hello'),
                // print(emailController.text),
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomeScreen(), // For Testing
                  ),
                ),
              },
              // press: getTextInputData(),
            ),
            AlreadyHaveAnAccountCheck(
              press: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SignupScreen(),
                  ),
                ),
              },
            ),
          ],
        ),
      ),
    );
  }

  // validationInput() {
  //   GetTextValue();
  // }
}
