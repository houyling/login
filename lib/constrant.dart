import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF6F35A5);
// Color kPrimaryColor = Color(0xFF6F35A5);
Color kPrimaryColor2 = getColorFromHex("#7b337d"); // testing with the hex color
const kPrimaryLightColor = Color(0xFFF1E6FF);
// const kPrimaryLightColor = Color(0xFFF1E6FF);

// http://paperheartdesign.com/blog/color-palette-awesome-space
// PURPLE HAZE
// The Orion Nebula is stunning in shades of purple!

// HEX CODES:
// #210535
// #430d4b
// #7b337d
// #c874b2
// #f5d5e0

Color getColorFromHex(String hexColor) {
  hexColor = hexColor.toUpperCase().replaceAll('#', '');

  if (hexColor.length == 6) {
    hexColor = 'FF' + hexColor;
  }

  return Color(int.parse(hexColor, radix: 16));
}
